# C

## Content

```
./C. C. Hunter:
C. C. Hunter - Shadow Falls - V1 Nascuta la miezul noptii 1.0 '{Vampiri}.docx
C. C. Hunter - Shadow Falls - V2 Trezita in zori de zi 0.99 '{Vampiri}.docx

./C. Ghiban:
C. Ghiban - Canta la Stupca o vioara 1.0 '{Biografie}.docx

./C. I. Bogdan:
C. I. Bogdan - Poduri suspendate 0.8 '{Literatura}.docx

./C. I. Christian:
C. I. Christian - Iugoslavia, sangeroasa destramare 1.0 '{Politica}.docx

./C. J. Cherryh:
C. J. Cherryh - Cassandra 0.99 '{SF}.docx
C. J. Cherryh - Statia orbitala a lumii de jos 1.0 '{SF}.docx

./C. J. Daugherty:
C. J. Daugherty - Night school 2.0 '{Tineret}.docx

./C. J. Tudor:
C. J. Tudor - Omul de creta 1.0 '{SF}.docx

./C. K. Stead:
C. K. Stead - Numele meu a fost Iuda 1.0 '{AventuraIstorica}.docx

./C. P. Snow:
C. P. Snow - Intoarcere acasa 1.0 '{Dragoste}.docx

./C. Plesa:
C. Plesa - Pestera de la Magura 0.99 '{Diverse}.docx

./C. R. Oliver:
C. R. Oliver - Taina lui Solomon 0.9 '{Spiritualitate}.docx

./C. W. Ceram:
C. W. Ceram - Secretul hititilor 1.0 '{MistersiStiinta}.docx

./Caitlin Clark:
Caitlin Clark - Tu iubita mea 0.9 '{Dragoste}.docx
Caitlin Clark - Vis si realitate 0.99 '{Dragoste}.docx

./Caitlin R. Kiernan:
Caitlin R. Kiernan - Calarind bivolul alb 1.0 '{SF}.docx

./Calatoriile fantastice ale lui Bill Gazon:
Calatoriile fantastice ale lui Bill Gazon - V01 Cersetorul orb 2.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V02 Temnita de sub apa 2.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V03 Nebunul de pe Soseaua Caravanelor 2.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V04 Piatra rosie 2.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V05 Zeul cu cornul de aur 2.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V06 Printre piratii chinezi 2.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V07 In tara Soarelui Rosu 2.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V08 Misterul mortii de pe vapor 2.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V09 Regele ceaiului 2.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V10 Avionul in pana 2.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V11 Corabia cu perle din Osaka 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V12 In tara elefantului alb 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V13 Betia de opiu 2.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V14 O noapte de groaza in Tokio 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V15 Varcolacul din Camciatca 2.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V16 In Siberia 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V17 Mica florareasa 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V18 Jocheul nenorocos 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V19 Calaretul ambulant 2.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V20 Cupa buclucasa 2.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V21 Cabellero cel blond 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V22 Mortul din camera galbena 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V23 O noapte in Cuba 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V24 Lautarul din Havana 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V25 Irlandezul incapatanat 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V26 Barca neagra 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V27 Patru saptamani calare 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V28 Strainul din Santa Fe 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V29 Infernul din San Francisco 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V30 Urmarire nocturna 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V31 Sase zile in barca 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V32 Cocosatul din St. Louis 2.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V33 Circul Manry 2.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V34 Detectiva din New York 2.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V35 Ploaie de dolari 2.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V36 Agrafa pescaritei 2.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V37 Atentatul de cale ferata 2.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V38 Coliba din mare 2.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V39 Mana ascunsa 2.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V40 Casa misterioasa 2.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V41 Mesterul din Toledo 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V42 Chipul de la fereastra 0.8 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V43 Misterul din Madrid 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V44 Trandafirul dansatoarei 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V45 Salvatorul din Valencia 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V46 Foc la bord 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V47 Atentat la Barcelona 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V48 O cursa perfida 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V49 Plutonul de executie 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V50 Drumul spre Madrid 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V51 Regasirea 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V52 In slujba lui Franco 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V53 In misiune secreta 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V54 Pe cerul Madridului 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V55 Ostatecul 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V56 Misterul din Algarve 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V57 Zbor peste Atlantic 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V58 Escala fortata 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V59 Insula misterelor 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V60 Grota ororilor 1.0 '{Tineret}.docx
Calatoriile fantastice ale lui Bill Gazon - V61 Pe cuterul piratilor 1.0 '{Tineret}.docx

./Calidasa:
Calidasa - Sacontala 0.99 '{Versuri}.docx

./Calinic Argatu:
Calinic Argatu - Traista cu stele 0.99 '{Diverse}.docx

./Calin Turcu:
Calin Turcu - Dosar OZN Romania 1.0 '{MistersiStiinta}.docx

./Calistrat Hogas:
Calistrat Hogas - Pe drumuri de munte 1.0 '{Natura}.docx

./Camelian Propinatiu:
Camelian Propinatiu - La un colt de cotitura 0.9 '{Diverse}.docx
Camelian Propinatiu - Oligopedagogia 0.8 '{Diverse}.docx

./Cameron West:
Cameron West - Pumnalul Medici 1.0 '{AventuraIstorica}.docx

./Camil Baciu:
Camil Baciu - Gradina zeilor 1.0 '{SF}.docx
Camil Baciu - Masina destinului 1.5 '{SF}.docx
Camil Baciu - Planeta cubica 1.0 '{SF}.docx
Camil Baciu - Revolta creierilor 2.0 '{SF}.docx

./Camilla Baresani:
Camilla Baresani - Iubire la imperfect 0.9 '{Literatura}.docx

./Camilla Grebe:
Camilla Grebe - Flickorna - V1 Gheata de sub picioarele ei 1.0 '{Thriller}.docx
Camilla Grebe - Flickorna - V2 Jurnalul disparitiei mele 1.0 '{Thriller}.docx

./Camilla Lackberg:
Camilla Lackberg - Colivia de aur 2.0 '{Literatura}.docx
Camilla Lackberg - Fjallbacka - V1 Printesa gheturilor 3.0 '{Politista}.docx
Camilla Lackberg - Fjallbacka - V2 Predicatorul 1.1 '{Politista}.docx
Camilla Lackberg - Fjallbacka - V3 Cioplitorul in piatra 1.1 '{Politista}.docx
Camilla Lackberg - Fjallbacka - V4 Piaza rea 1.1 '{Politista}.docx
Camilla Lackberg - Fjallbacka - V5 Copilul german 1.1 '{Politista}.docx
Camilla Lackberg - Fjallbacka - V6 Sirena 1.1 '{Politista}.docx
Camilla Lackberg - Fjallbacka - V7 Paznicul farului 1.1 '{Politista}.docx
Camilla Lackberg - Fjallbacka - V8 Fauritoarea de ingeri 1.1 '{Politista}.docx
Camilla Lackberg - Fjallbacka - V9 Imblanzitorul de lei 1.0 '{Politista}.docx
Camilla Lackberg - Fjallbacka - V10 Vrajitoarea 1.0 '{Politista}.docx

./Camille Laurens:
Camille Laurens - Femeia inventata 1.0 '{Romance}.docx

./Camillo Boito:
Camillo Boito - Din caietul secret al contesei Livia 0.8 '{Diverse}.docx

./Camilo Cela:
Camilo Cela - Familia lui Pascual Duarte 0.99 '{Literatura}.docx

./Camil Petrescu:
Camil Petrescu - In vreme de razboi 0.8 '{ClasicRo}.docx
Camil Petrescu - Patul lui Procust 1.0 '{ClasicRo}.docx
Camil Petrescu - Ultima noapte de dragoste, intaia noapte de razboi. Patul lui Procust 1.1 '{ClasicRo}.docx
Camil Petrescu - Ultima noapte de dragoste, intaia noapte de razboi 1.0 '{ClasicRo}.docx
Camil Petrescu - Un om intre oameni V1 1.0 '{ClasicRo}.docx

./Camil Roguski:
Camil Roguski - Ceausescu - Adevaruri interzise 0.9 '{Comunism}.docx

./Campbell Black:
Campbell Black - Indiana Jones si Taina Tainelor 1.0 '{Aventura}.docx

./Candy Moore:
Candy Moore - Atractie magica 0.99 '{Dragoste}.docx
Candy Moore - Minunile dragostei 0.99 '{Dragoste}.docx
Candy Moore - Nu-ti fie teama de iubire 0.99 '{Romance}.docx
Candy Moore - Sarutul recompensa 0.9 '{Romance}.docx

./Cara Delevingne:
Cara Delevingne - Mirror, mirror 1.0 '{Literatura}.docx

./Cara Elliott:
Cara Elliott - Tentatii periculoase 0.99 '{Romance}.docx

./Caragh M. O'brien:
Caragh M. O'brien - Maestrul mangaierilor 0.9 '{Dragoste}.docx
Caragh M. O'brien - Rasare Steaua Polara 0.9 '{Dragoste}.docx

./Care Santos:
Care Santos - Aerul pe care il respiri 1.0 '{Literatura}.docx
Care Santos - Incaperi ferecate 1.0 '{Literatura}.docx
Care Santos - Pofta de ciocolata 1.0 '{Literatura}.docx

./Care Santos & Francesc Miralles:
Care Santos & Francesc Miralles - Cel mai frumos loc din lume e chiar aici 1.0 '{Literatura}.docx

./Carey Ken:
Carey Ken - Transmisiunile semintiei stelare 0.99 '{Spiritualitate}.docx

./Carla Guelfenbein:
Carla Guelfenbein - Cu tine in departare 1.0 '{Literatura}.docx

./Carl Gustav Jung:
Carl Gustav Jung - Arhetipurile si inconstientul colectiv 0.9 '{Filozofie}.docx
Carl Gustav Jung - Despre cunoasterea lui Dumnezeu 0.9 '{Filozofie}.docx

./Carl Hiaasen:
Carl Hiaasen - Strip tease 1.0 '{Politista}.docx

./Carl Nagaitis:
Carl Nagaitis - Rapiri in spatiu 0.7 '{MistersiStiinta}.docx

./Carlo Cassola:
Carlo Cassola - Logodnica lui Bube 1.0 '{Dragoste}.docx

./Carlo Collodi:
Carlo Collodi - Aventurile lui Pinocchio 0.99 '{Tineret}.docx

./Carlo Frabetti:
Carlo Frabetti - Cartea iad 0.9 '{Diverse}.docx

./Carlo Goldoni:
Carlo Goldoni - Badaranii 1.0 '{Teatru}.docx
Carlo Goldoni - Cafeneaua 1.0 '{Teatru}.docx
Carlo Goldoni - Galcevele din Chioggia 1.0 '{Teatru}.docx
Carlo Goldoni - Sluga la doi stapani 1.0 '{Teatru}.docx

./Carlo Lucarelli:
Carlo Lucarelli - Ziua lupului 1.0 '{Politista}.docx

./Carlo Manzoni:
Carlo Manzoni - Aventurile unui detectiv particular 2.0 '{Politista}.docx
Carlo Manzoni - E acasa domnul Brambilla 1.0 '{Politista}.docx
Carlo Manzoni - O fac zob! Calibrul 22 1.0 '{Politista}.docx

./Carlo Martigli:
Carlo Martigli - Ultimul custode 1.0 '{AventuraIstorica}.docx

./Carlos Castaneda:
Carlos Castaneda - Calea luptatorului neinfricat care aspira catre Dumnezeu 0.99 '{Spiritualitate}.docx
Carlos Castaneda - Citate 0.99 '{Spiritualitate}.docx
Carlos Castaneda - Don Juan - V1 Invataturile lui Don Juan 1.0 '{Spiritualitate}.docx
Carlos Castaneda - Don Juan - V2 Cealalta realitate 1.0 '{Spiritualitate}.docx
Carlos Castaneda - Don Juan - V3 Calatorie la Ixtlan 1.0 '{Spiritualitate}.docx
Carlos Castaneda - Don Juan - V4 Povestiri despre putere 1.0 '{Spiritualitate}.docx
Carlos Castaneda - Don Juan - V5 Al doilea cerc de putere 1.0 '{Spiritualitate}.docx
Carlos Castaneda - Don Juan - V6 Darul vulturului 1.0 '{Spiritualitate}.docx
Carlos Castaneda - Don Juan - V7 Focul launtric 1.0 '{Spiritualitate}.docx
Carlos Castaneda - Don Juan - V8 Puterea tacerii 1.0 '{Spiritualitate}.docx
Carlos Castaneda - Don Juan - V9 Arta visatului 1.0 '{Spiritualitate}.docx
Carlos Castaneda - Don Juan - V10 Pase magice 0.9 '{Spiritualitate}.docx
Carlos Castaneda - Don Juan - V11 Latura activa a infinitatii 0.9 '{Spiritualitate}.docx

./Carlos Fuentes:
Carlos Fuentes - Batranul gringo 0.99 '{Literatura}.docx
Carlos Fuentes - Constancia 0.9 '{Literatura}.docx
Carlos Fuentes - Diana sau zeita solitara a vanatorii 0.99 '{Literatura}.docx
Carlos Fuentes - Gringo Viejo 0.99 '{Literatura}.docx
Carlos Fuentes - Moartea lui Artemio Cruz 1.0 '{Literatura}.docx
Carlos Fuentes - Noile ispravi si necazuri ale lui Lazarillo de Tormes 0.7 '{Literatura}.docx
Carlos Fuentes - O companie nelinistitoare 0.7 '{Literatura}.docx
Carlos Fuentes - Toate pisicile sunt negre 0.6 '{Literatura}.docx

./Carlos Ruiz Zafon:
Carlos Ruiz Zafon - Cimitirul Cartilor Uitate - V4 Labirintul spiritelor 0.9 '{Literatura}.docx
Carlos Ruiz Zafon - Jocul ingerului 1.0 '{Tineret}.docx
Carlos Ruiz Zafon - Marina 1.0 '{Tineret}.docx
Carlos Ruiz Zafon - Printul din negura 1.0 '{Tineret}.docx
Carlos Ruiz Zafon - Prizonierul cerului 1.0 '{Tineret}.docx
Carlos Ruiz Zafon - Umbra vantului 1.0 '{Tineret}.docx

./Carlos Warter:
Carlos Warter - Amintirile sufletului 0.7 '{Spiritualitate}.docx

./Carl Sagan:
Carl Sagan - Balaurii raiului 1.0 '{Psihologie}.docx
Carl Sagan - Contact 2.0 '{SF}.docx
Carl Sagan - Creierul lui Broca 1.0 '{Filozofie}.docx
Carl Sagan - Lumea si demonii ei 1.0 '{MistersiStiinta}.docx

./Carl Sandburg:
Carl Sandburg - Povesti din tara Rutabaga 0.9 '{BasmesiPovesti}.docx

./Carly Phillips:
Carly Phillips - Burlacul 1.0 '{Romance}.docx

./Carmen Chirea Ungurean:
Carmen Chirea Ungurean - Gaston Bachelard si posteritatea sa critica 0.99 '{Diverse}.docx

./Carmen Emanuela Simiras:
Carmen Emanuela Simiras - O iubire asa de mare 0.9 '{Dragoste}.docx

./Carmen Furtuna:
Carmen Furtuna - Sociologie generala 0.99 '{Diverse}.docx

./Carmen Harra:
Carmen Harra - Decodificarea destinului 0.7 '{Spiritualitate}.docx
Carmen Harra - Legile spirituale universale V1 0.5 '{Spiritualitate}.docx

./Carmen Laforet:
Carmen Laforet - Neant 1.0 '{Literatura}.docx

./Carmen Martin Gaite:
Carmen Martin Gaite - Logodna Gertrudei 0.9 '{Dragoste}.docx

./Carmen Vioreanu:
Carmen Vioreanu - Dincolo 0.9 '{Teatru}.docx

./Carol Birch:
Carol Birch - Menajeria lui Jamrach 1.0 '{Aventura}.docx

./Carol Bogolin:
Carol Bogolin - Jocul dragostei 0.9 '{Dragoste}.docx
Carol Bogolin - O casatorie salvata 0.99 '{Dragoste}.docx

./Carol Cassella:
Carol Cassella - Oxigen 0.99 '{Literatura}.docx

./Carole Dean:
Carole Dean - Cand apare curcubeul 0.9 '{Romance}.docx
Carole Dean - Dinastia 0.99 '{Dragoste}.docx
Carole Dean - Fascinanata dedublare 0.99 '{Dragoste}.docx
Carole Dean - Insula din Caraibe 0.99 '{Dragoste}.docx
Carole Dean - Secrete bine ascunse 0.99 '{Dragoste}.docx
Carole Dean - Un barbat numit Blue 0.99 '{Dragoste}.docx

./Carole Mortimer:
Carole Mortimer - Lady 1.0 '{Romance}.docx

./Carole Stivers:
Carole Stivers - Codul Vietii 1.0 '{Literatura}.docx

./Carol I al Romaniei:
Carol I al Romaniei - Jurnal V1 1.0 '{Biografie}.docx

./Caroline Adams:
Caroline Adams - Invaluita in intuneric 1.0 '{Romance}.docx

./Caroline Cottrell:
Caroline Cottrell - Atingere de matase 0.9 '{Dragoste}.docx
Caroline Cottrell - Sotie sau prietena 0.99 '{Dragoste}.docx
Caroline Cottrell - Walparo 0.99 '{Dragoste}.docx

./Caroline Farr:
Caroline Farr - Castelul de granit 0.99 '{Dragoste}.docx
Caroline Farr - Umbrele de altadata 0.99 '{Romance}.docx

./Caroline Fisher:
Caroline Fisher - Amintirea unei veri frumoase 0.99 '{Romance}.docx
Caroline Fisher - Calatorie in Africa 0.9 '{Dragoste}.docx
Caroline Fisher - Din toata inima 0.9 '{Romance}.docx
Caroline Fisher - Frontiere nebanuite 0.99 '{Romance}.docx

./Caroline Gayet:
Caroline Gayet - Condurul de sticla 0.9 '{Romance}.docx
Caroline Gayet - Intalnire la Tripoli 0.9 '{Dragoste}.docx

./Caroline Myss:
Caroline Myss - De ce nu ne vindecam 0.7 '{Sanatate}.docx

./Caroline Pitcher:
Caroline Pitcher - Prajitura de la ora 11 0.99 '{Tineret}.docx

./Carolyn Brown:
Carolyn Brown - Secrete si sperante 1.0 '{Dragoste}.docx

./Carolyn Ives Gilman:
Carolyn Ives Gilman - Okanoggan falls 1.0 '{Diverse}.docx

./Carolyn Maccullough:
Carolyn Maccullough - Witch - V1 Cand esti vrajitoare 1.0 '{Supranatural}.docx
Carolyn Maccullough - Witch - V2 Ramai vesnic vrajitoare 1.0 '{Supranatural}.docx

./Carrie Jones:
Carrie Jones - Setea 1.0 '{Tineret}.docx

./Carson Mccullers:
Carson Mccullers - Inima e un vanator singuratic 0.7 '{Thriller}.docx
Carson Mccullers - In vara aceea verde 1.0 '{Thriller}.docx
Carson Mccullers - Rasfrangeri intr-un ochi de aur 1.1 '{Thriller}.docx

./Cartea Lui Enoh:
Cartea Lui Enoh - Versiunea etiopiana 0.9 '{Spiritualitate}.docx

./Carti Populare Romanesti:
Carti Populare Romanesti - Istoria lui Alexandru Macedon. Pildele si invataturile lui Esop 0.8 '{Istorie}.docx

./Caryl Ferey:
Caryl Ferey - Zulu 1.0 '{Politista}.docx

./Caryl Wilson:
Caryl Wilson - In seara asta si pe vecie 1.0 '{Romance}.docx

./Casian Balabasciuc:
Casian Balabasciuc - Cantecul Sihlei 0.99 '{Diverse}.docx
Casian Balabasciuc - Motanul 0.99 '{Diverse}.docx

./Cassandra Clare:
Cassandra Clare - Dispozitive Infernale - V1 Ingerul mecanic 1.0 '{Supranatural}.docx
Cassandra Clare - Dispozitive Infernale - V2 Printul mecanic 1.0 '{Supranatural}.docx
Cassandra Clare - Dispozitive Infernale - V3 Printesa mecanica 1.0 '{Supranatural}.docx
Cassandra Clare - Instrumente Mortale - V1 Orasul oaselor 1.0 '{Supranatural}.docx
Cassandra Clare - Instrumente Mortale - V2 Orasul de cenusa 1.0 '{Supranatural}.docx
Cassandra Clare - Instrumente Mortale - V3 Orasul de sticla 1.0 '{Supranatural}.docx
Cassandra Clare - Instrumente Mortale - V4 Orasul ingerilor cazuti 1.0 '{Supranatural}.docx
Cassandra Clare - Instrumente Mortale - V5 Orasul sufletelor pierdute 1.0 '{Supranatural}.docx
Cassandra Clare - Instrumente Mortale - V6 Orasul focului ceresc 0.7 '{Supranatural}.docx
Cassandra Clare - Uneltiri Intunecate - V1 Doamna de la miezul noptii 0.9 '{Supranatural}.docx
Cassandra Clare - Uneltiri Intunecate - V2 Stapanul umbrelor 0.9 '{Supranatural}.docx

./Catalin Bizdadea:
Catalin Bizdadea - Timpul este doar o masina 2.0 '{SF}.docx

./Catalin Cofaru:
Catalin Cofaru - Fortareata 0.99 '{SF}.docx
Catalin Cofaru - Insecte 0.99 '{SF}.docx

./Catalin Dan Carnaru:
Catalin Dan Carnaru - Energia pentru toti V1 0.6 '{Stiinta}.docx
Catalin Dan Carnaru - Energia pentru toti V2 0.8 '{Stiinta}.docx
Catalin Dan Carnaru - Energia pentru toti V3 0.8 '{Stiinta}.docx

./Catalin Dorian Florescu:
Catalin Dorian Florescu - Drumul scurt spre casa 0.8 '{Literatura}.docx
Catalin Dorian Florescu - Jacob se hotaraste sa iubeasca 1.0 '{Literatura}.docx
Catalin Dorian Florescu - Maseurul orb 0.99 '{Literatura}.docx
Catalin Dorian Florescu - Vremea minunilor 0.99 '{Literatura}.docx

./Catalin Gavriliu:
Catalin Gavriliu - Andreea Paula 0.9 '{Teatru}.docx
Catalin Gavriliu - Domnul Dima 0.99 '{Literatura}.docx
Catalin Gavriliu - Entu 0.8 '{Literatura}.docx
Catalin Gavriliu - Ghita 0.9 '{Literatura}.docx
Catalin Gavriliu - Kanel 0.99 '{Literatura}.docx

./Catalin Hidegcuti:
Catalin Hidegcuti - A doua epoca intunecata 0.99 '{SF}.docx
Catalin Hidegcuti - Amintiri dintr-o lume departe 0.9 '{SF}.docx
Catalin Hidegcuti - Anul 10.000 0.99 '{SF}.docx
Catalin Hidegcuti - Calea dacilor 0.9 '{SF}.docx
Catalin Hidegcuti - Citadela asociatiei 0.9 '{SF}.docx
Catalin Hidegcuti - Extraplanarii 0.9 '{SF}.docx
Catalin Hidegcuti - Moarte pentru onoare. Un targ cinstit 0.9 '{SF}.docx
Catalin Hidegcuti - Munca dragunilor 0.9 '{SF}.docx
Catalin Hidegcuti - Nebunii lui Arrianus 0.8 '{SF}.docx
Catalin Hidegcuti - Pasul craiului de munte 0.9 '{SF}.docx
Catalin Hidegcuti - Raidul micutilor 0.9 '{SF}.docx
Catalin Hidegcuti - Saga Misticului - Gasirea 0.8 '{SF}.docx
Catalin Hidegcuti - Schimbare de management 0.8 '{SF}.docx
Catalin Hidegcuti - Stapanul plantelor 0.9 '{SF}.docx
Catalin Hidegcuti - Ultima aventura a bandei lui Darien Ka 0.9 '{SF}.docx
Catalin Hidegcuti - Unde s-a ascuns Dumnezeu 0.9 '{SF}.docx

./Catalin Ionescu:
Catalin Ionescu - Crima perfecta de gradul 4 0.8 '{SF}.docx
Catalin Ionescu - Troia, intamplator 0.99 '{SF}.docx

./Catalin Maxim:
Catalin Maxim - Jucarii 0.99 '{Literatura}.docx

./Catalin Petrescu:
Catalin Petrescu - Iulia 2.0 '{Teatru}.docx

./Cath Crowley:
Cath Crowley - Cuvinte in albastru intens 1.0 '{Dragoste}.docx
Cath Crowley - Graffiti Moon 1.0 '{Aventura}.docx

./Catherine Clark:
Catherine Clark - Mostenitorul fermei 0,99 '{Dragoste}.docx

./Catherine Clement:
Catherine Clement - Martin si Hannah 1.0 '{Dragoste}.docx

./Catherine Cusset:
Catherine Cusset - Un viitor stralucit 1.0 '{Biografie}.docx

./Catherine Durandin:
Catherine Durandin - Moartea Ceausestilor 1.0 '{Comunism}.docx

./Catherine Fisher:
Catherine Fisher - Carcera 1.0 '{SF}.docx

./Catherine Isaac:
Catherine Isaac - Aici, acum, mereu 1.0 '{Literatura}.docx

./Catherine Mulvany:
Catherine Mulvany - Acvamarin 0.99 '{Dragoste}.docx

./Catherine Robinson:
Catherine Robinson - Un seducator parsiv 0.99 '{Dragoste}.docx

./Catherine Sellers:
Catherine Sellers - Escapada norvegiana 0.99 '{Romance}.docx
Catherine Sellers - Sub stancile din Acapulco 0.99 '{Romance}.docx

./Catherine Steadman:
Catherine Steadman - Ceva in apa 1.0 '{SF}.docx

./Catherine Wheelwright:
Catherine Wheelwright - Tanar si nelinistit 0.99 '{Dragoste}.docx

./Cathy Andrews:
Cathy Andrews - Barbatul din Tripoli 1.0 '{Romance}.docx

./Cathy Cassidy:
Cathy Cassidy - Pasiunea lui Cherry 0.99 '{Tineret}.docx

./Cathy Spellman:
Cathy Spellman - Binecuvantat fie copilul 1.0 '{Literatura}.docx

./Cat Weatherill:
Cat Weatherill - Ashenpeake - V1 Barkbelly 1.0 '{Supranatural}.docx
Cat Weatherill - Magie dezlantuita 1.0 '{Supranatural}.docx

./Cay Dancer:
Cay Dancer - Golful norilor 0.99 '{Dragoste}.docx

./Ceapaev:
Ceapaev - Furmanov 1.0 '{Literatura}.docx

./Cecelia Ahern:
Cecelia Ahern - Cadoul 1.0 '{Romance}.docx
Cecelia Ahern - Cartea viitorului 0.9 '{Romance}.docx
Cecelia Ahern - Defecti 1.0 '{Romance}.docx
Cecelia Ahern - P.S. Te iubesc 1.0 '{Romance}.docx
Cecelia Ahern - Perfecti 1.0 '{Romance}.docx
Cecelia Ahern - Prietenul nevazut 2.0 '{Romance}.docx
Cecelia Ahern - ROAR 1.0 '{Romance}.docx

./Cecile Fox:
Cecile Fox - Mama pentru Jenny 0.9 '{Romance}.docx

./Cecil Freeman Gregg:
Cecil Freeman Gregg - Misterul de la Cliff House 2.0 '{Thriller}.docx

./Cecilia Dudu:
Cecilia Dudu - Coniac 3 secole 1.0 '{SF}.docx
Cecilia Dudu - Scarabeul lui Rasid 1.0 '{SF}.docx

./Cecilia Dudu & Dumitru Todericiu:
Cecilia Dudu & Dumitru Todericiu - Foc pe cer in Ursa Mica 1.0 '{SF}.docx

./Cees Nooteboom:
Cees Nooteboom - Pierdutul paradis 1.0 '{Literatura}.docx
Cees Nooteboom - Urmatoarea poveste 0.7 '{Literatura}.docx

./Celeste Ng:
Celeste Ng - Mici focuri pretutindeni 1.0 '{Thriller}.docx
Celeste Ng - Tot ce nu ti-am spus 1.0 '{Thriller}.docx

./Celine Pullman:
Celine Pullman - Domnisoara de onoare 0.9 '{Dragoste}.docx

./Cella Delavrancea:
Cella Delavrancea - O vara ciudata 1.0 '{Dragoste}.docx

./Cella Serghi:
Cella Serghi - Aceasta dulce povara, tineretea 1.0 '{Dragoste}.docx
Cella Serghi - Gentiane 1.0 '{Dragoste}.docx
Cella Serghi - In cautarea somnului urias 1.0 '{Tineret}.docx
Cella Serghi - Iubiri paralele 1.0 '{Dragoste}.docx
Cella Serghi - Mirona 0.9 '{Dragoste}.docx
Cella Serghi - Panza de paianjen 1.0 '{Dragoste}.docx
Cella Serghi - Pe firul de paianjen al memoriei 1.0 '{Dragoste}.docx

./Cesare Pavese:
Cesare Pavese - Casa de pe colina 0.9 '{Literatura}.docx

./Cezar Boliac:
Cezar Boliac - Meditatii 1.0 '{Versuri}.docx

./Cezar Login:
Cezar Login - Cantarile lui Moise 0.9 '{Religie}.docx

./Cezar Paul Badescu:
Cezar Paul Badescu - Luminita, mon amour 0.8 '{Literatura}.docx

./Cezar Petrescu:
Cezar Petrescu - 1907 V1 0.9 '{ClasicRo}.docx
Cezar Petrescu - 1907 V2 0.9 '{ClasicRo}.docx
Cezar Petrescu - 1907 V3 0.9 '{ClasicRo}.docx
Cezar Petrescu - Adapostul Sobolia 1.0 '{ClasicRo}.docx
Cezar Petrescu - Apostol 1.0 '{ClasicRo}.docx
Cezar Petrescu - Aurul negru 1.0 '{ClasicRo}.docx
Cezar Petrescu - Baletul mecanic 0.9 '{ClasicRo}.docx
Cezar Petrescu - Calea Victoriei. Dumineca orbului 1.0 '{ClasicRo}.docx
Cezar Petrescu - Carlton 1.0 '{ClasicRo}.docx
Cezar Petrescu - Cei trei regi 1.0 '{ClasicRo}.docx
Cezar Petrescu - Cocart si bomba atomica 1.0 '{ClasicRo}.docx
Cezar Petrescu - Dumineca orbului 2.0 '{ClasicRo}.docx
Cezar Petrescu - Flori de ghiata 1.0 '{ClasicRo}.docx
Cezar Petrescu - Fram ursul polar 1.0 '{ClasicRo}.docx
Cezar Petrescu - Greta Garbo 1.0 '{ClasicRo}.docx
Cezar Petrescu - Intunecare V1 2.0 '{ClasicRo}.docx
Cezar Petrescu - Intunecare V2 2.0 '{ClasicRo}.docx
Cezar Petrescu - La Paradis General. Miss Romania 0.9 '{ClasicRo}.docx
Cezar Petrescu - Naluca si alte povestiri 1.0 '{ClasicRo}.docx
Cezar Petrescu - Oameni de ieri, oameni de azi, oameni de maine 0.9 '{ClasicRo}.docx
Cezar Petrescu - Omul de zapada 0.9 '{ClasicRo}.docx
Cezar Petrescu - Omul din vis 1.0 '{ClasicRo}.docx
Cezar Petrescu - Oras patriarhal 1.0 '{ClasicRo}.docx
Cezar Petrescu - Pif, paf, puf si alte povestiri 1.0 '{ClasicRo}.docx
Cezar Petrescu - Plecat fara adresa 0.9 '{ClasicRo}.docx
Cezar Petrescu - Proza fantastica 1.0 '{ClasicRo}.docx
Cezar Petrescu - Romanul lui Eminescu - V1 Luceafarul 1.0 '{ClasicRo}.docx
Cezar Petrescu - Romanul lui Eminescu - V2 Nirvana 1.0 '{ClasicRo}.docx
Cezar Petrescu - Romanul lui Eminescu - V3 Carmen Saeculare 1.0 '{ClasicRo}.docx
Cezar Petrescu - Scrisorile unui razes 1.0 '{ClasicRo}.docx
Cezar Petrescu - Simfonia fantastica 0.9 '{ClasicRo}.docx

./Cezar Petrescu & Mihai Novicov:
Cezar Petrescu & Mihai Novicov - Nepotii gornistului 1.0 '{ClasicRo}.docx

./Chang Rae Lee:
Chang Rae Lee - Zbor peste Long Island 0.99 '{Literatura}.docx

./Charlaine Harris:
Charlaine Harris - Vampirii Sudului - V1 Morti pana la apus 1.0 '{Vampiri}.docx
Charlaine Harris - Vampirii Sudului - V2 Moartea la Dallas 1.0 '{Vampiri}.docx
Charlaine Harris - Vampirii Sudului - V3 Clubul mortilor 1.0 '{Vampiri}.docx
Charlaine Harris - Vampirii Sudului - V4 Moarta pentru toti 0.9 '{Vampiri}.docx
Charlaine Harris - Vampirii Sudului - V5 Mort de-a binelea 1.0 '{Vampiri}.docx
Charlaine Harris - Vampirii Sudului - V6 Categoric moarta 1.0 '{Vampiri}.docx
Charlaine Harris - Vampirii Sudului - V7 Morti cu totii 1.0 '{Vampiri}.docx
Charlaine Harris - Vampirii Sudului - V8 Mai rau decat moartea 1.0 '{Vampiri}.docx
Charlaine Harris - Vampirii Sudului - V9 Mort si-ngropat 1.0 '{Vampiri}.docx
Charlaine Harris - Vampirii Sudului - V10 Moarte in familie 1.0 '{Vampiri}.docx
Charlaine Harris - Vampirii Sudului - V11 Capcana mortala 2.0 '{Vampiri}.docx
Charlaine Harris - Vampirii Sudului - V12 Atingerea mortii - Povestiri cu Sookie Stockhouse 1.0 '{Vampiri}.docx

./Charles Belfoure:
Charles Belfoure - Arhitectul parizian 1.0 '{Suspans}.docx

./Charles Berlitz:
Charles Berlitz - Misterul lumilor uitate 1.0 '{MistersiStiinta}.docx
Charles Berlitz - Triunghiul Bermudelor - Incredibila poveste a disparitiilor misterioase 0.7 '{MistersiStiinta}.docx

./Charles Bukowski:
Charles Bukowski - Factotum 0.99 '{Literatura}.docx
Charles Bukowski - Femei 1.0 '{Literatura}.docx
Charles Bukowski - Posta 1.0 '{Literatura}.docx

./Charles de Coster:
Charles de Coster - Thyl Ulenspiegel 2.0 '{Tineret}.docx

./Charles Deslys:
Charles Deslys - Zingara 1.0 '{Dragoste}.docx

./Charles Dickens:
Charles Dickens - Casa umbrelor 0.7 '{ClasicSt}.docx
Charles Dickens - David Copperfield V1 1.0 '{ClasicSt}.docx
Charles Dickens - David Copperfield V2 1.0 '{ClasicSt}.docx
Charles Dickens - David Copperfield V3 0.9 '{ClasicSt}.docx
Charles Dickens - Documentele postume ale Clubului Pickwick 1.0 '{ClasicSt}.docx
Charles Dickens - Impresii din Italia 1.0 '{ClasicSt}.docx
Charles Dickens - Istoria Angliei pentru copii V1 0.5 '{ClasicSt}.docx
Charles Dickens - Marile sperante 1.0 '{ClasicSt}.docx
Charles Dickens - Misterul lui Edwin Drood 1.0 '{ClasicSt}.docx
Charles Dickens - Oliver Twist 1.1 '{ClasicSt}.docx
Charles Dickens - Pickwick V1 0.9 '{ClasicSt}.docx
Charles Dickens - Pickwick V2 0.9 '{ClasicSt}.docx
Charles Dickens - Poveste despre doua orase 1.0 '{ClasicSt}.docx
Charles Dickens - Povestiri 1.0 '{ClasicSt}.docx
Charles Dickens - Schitele lui Boz 0.7 '{ClasicSt}.docx
Charles Dickens - Semnalizatorul 0.99 '{ClasicSt}.docx
Charles Dickens - Timpuri grele 1.0 '{ClasicSt}.docx
Charles Dickens - Un colind de Craciun 3.0 '{ClasicSt}.docx

./Charles Diehl:
Charles Diehl - Theodora, imparateasa Bizantului 0.9 '{Istorie}.docx

./Charles E. Fritch:
Charles E. Fritch - Buddy Else intalneste molia 0.99 '{SF}.docx

./Charles F. Haanel:
Charles F. Haanel - Sistemul maestrului 0.7 '{DezvoltarePersonala}.docx

./Charles Frazier:
Charles Frazier - Cold mountain 1.0 '{Literatura}.docx

./Charles Haddon Spurgeon:
Charles Haddon Spurgeon - Eu sunt Domnul medicul tau 0.9 '{Religie}.docx
Charles Haddon Spurgeon - Inaintea portii 0.9 '{Religie}.docx
Charles Haddon Spurgeon - Viata din belsug 0.9 '{Religie}.docx

./Charles Lamb & Mary Lamb:
Charles Lamb & Mary Lamb - Povestiri dupa Shakespeare 1.0 '{ClasicSt}.docx

./Charles Leblanc:
Charles Leblanc - Rex P. ameninta 1.0 '{Detectiv}.docx

./Charles Maclean:
Charles Maclean - Chipul intunericului 1.0 '{Thriller}.docx

./Charles Martin:
Charles Martin - Cand greierii plang 1.0 '{Literatura}.docx
Charles Martin - Invaluiti in ploaie 1.1 '{Dragoste}.docx
Charles Martin - Muntele dintre noi 1.0 '{Literatura}.docx

./Charles Portis:
Charles Portis - Adevaratul curaj 1.0 '{Western}.docx

./Charles Robert Maturin:
Charles Robert Maturin - Castelul din Leixlip 0.8 '{Literatura}.docx

./Charles Stross:
Charles Stross - Eschaton - V1 Spatiul singularitatii 1.0 '{SF}.docx
Charles Stross - Eschaton - V2 Rasarit de fier 1.0 '{SF}.docx

./Charlie Jane Anders:
Charlie Jane Anders - Orasul de la miezul noptii 1.0 '{SF}.docx
Charlie Jane Anders - Toate pasarile din cer 1.0 '{Literatura}.docx

./Charlie W. Shedd:
Charlie W. Shedd - Scrisori Caterinei. Sfaturi unei tinere casatorite 0.9 '{DezvoltarePersonala}.docx

./Charlotte Bronte:
Charlotte Bronte - Jane Eyre - V1 Jane Eyre 1.0 '{ClasicSt}.docx
Charlotte Bronte - Jane Eyre - V2 Alaturi de Edward 1.0 '{ClasicSt}.docx
Charlotte Bronte - Shirley 1.0 '{ClasicSt}.docx
Charlotte Bronte - Villette 1.0 '{ClasicSt}.docx

./Charlotte Hughes:
Charlotte Hughes - De ce m-ai parasit 0.99 '{Dragoste}.docx
Charlotte Hughes - Din surpriza in surpriza 0.9 '{Dragoste}.docx
Charlotte Hughes - Se cauta sot 0.99 '{Romance}.docx
Charlotte Hughes - Vanatorul de inimi 0.99 '{Romance}.docx

./Charlotte Hugues:
Charlotte Hugues - Reteta fericirii 0.8 '{Dragoste}.docx

./Charlotte Lamb:
Charlotte Lamb - Carnaval la Venetia 1.0 '{Romance}.docx

./Charlotte Mann:
Charlotte Mann - Stapana castelului Vandermour 0.9 '{Romance}.docx

./Charlotte Paine:
Charlotte Paine - Paradoxul iubirii 0.99 '{Dragoste}.docx

./Charlotte Ussel:
Charlotte Ussel - Potecile din Katmandu 0.99 '{Dragoste}.docx

./Chelsea Cain:
Chelsea Cain - Sheridan & Lowell - V1 In inima raului 1.0 '{Horror}.docx

./Chelsea Quinn Yarbro:
Chelsea Quinn Yarbro - Hotel Transilvania 0.9 '{Vampiri}.docx
Chelsea Quinn Yarbro - Noptile bestiei 0.99 '{Vampiri}.docx

./Chimamanda Ngozi Adiche:
Chimamanda Ngozi Adiche - Hibiscus purpuriu 1.0 '{Literatura}.docx

./China Mieville:
China Mieville - Noul Crobuzon - V1 Statia pierzaniei 2.0 '{SF}.docx
China Mieville - Noul Crobuzon - V2 Cicatricea 2.0 '{SF}.docx
China Mieville - Noul Crobuzon - V3 Consiliul de fier 1.0 '{SF}.docx
China Mieville - Regele sobolan 2.0 '{SF}.docx
China Mieville - The city & the city 1.0 '{SF}.docx

./Ching Hai:
Ching Hai - Am venit sa va iau acasa 1.0 '{Spiritualitate}.docx
Ching Hai - Cheia iluminarii spirituale imediate 0.9 '{Spiritualitate}.docx
Ching Hai - Daunele imense aduse de consumul de produse animale, alcool si tutun 0.8 '{Spiritualitate}.docx
Ching Hai - De la criza la pace. Calea vegana 0.8 '{Spiritualitate}.docx
Ching Hai - Interviu cu maestra suprema Ching Hai la Johannesburg, Africa de sud - 28 noiembrie 1999 0.9 '{Spiritualitate}.docx
Ching Hai - Metoda Quan Yin V1 - Maestra suprema Ching Hai 0.9 '{Spiritualitate}.docx
Ching Hai - Metoda Quan Yin V2 - Metoda Quan Yin 0.9 '{Spiritualitate}.docx
Ching Hai - Metoda Quan Yin V3 - Calatoria spirituala 0.9 '{Spiritualitate}.docx
Ching Hai - Metoda Quan Yin V4 - Insemnatatea adevarata a Ahimsei 0.9 '{Spiritualitate}.docx
Ching Hai - Revista 209 0.9 '{Spiritualitate}.docx

./Chinua Achebe:
Chinua Achebe - O lume se destrama 0.99 '{Literatura}.docx

./Chiril Tricolici:
Chiril Tricolici - Calimera 1.0 '{Literatura}.docx
Chiril Tricolici - Placerile jocului 1.0 '{Literatura}.docx
Chiril Tricolici - Rolls Royce 1.0 '{Dragoste}.docx
Chiril Tricolici - Un dolar, doi dolari. Valetul de trefla 0.9 '{Politista}.docx

./Chloe Benjamin:
Chloe Benjamin - Imortalistii 1.0 '{Literatura}.docx

./Chloe Esposito:
Chloe Esposito - Nebuna, Rea si Periculoasa - V1 Nebuna 1.0 '{Suspans}.docx
Chloe Esposito - Nebuna, rea si periculoasa - V2 Rea 1.0 '{Suspans}.docx

./Chloe Neil:
Chloe Neil - Elita Intunecata - V1 Blestemul focului 0.99 '{Diverse}.docx
Chloe Neil - Elita Intunecata - V2 Hex sau puterea farmecelor 0.99 '{Diverse}.docx

./Chretien de Troyes:
Chretien de Troyes - Cavalerul Lancelot 1.0 '{AventuraIstorica}.docx
Chretien de Troyes - Yvain cavalerul cu leul 1.0 '{AventuraIstorica}.docx

./Chris Bohjalian:
Chris Bohjalian - Dubla constrangere 0.99 '{Literatura}.docx
Chris Bohjalian - Inchideti ochii. Luati-va de mana 1.0 '{Literatura}.docx

./Chris Bunch & Alan Cole:
Chris Bunch & Alan Cole - Rafuiala regilor 1.0 '{ActiuneRazboi}.docx

./Chris Buster Morris:
Chris Buster Morris - Pagoda musonului 1.0 '{ActiuneComando}.docx
Chris Buster Morris - Sindromul Tirpitz 0.8 '{ActiuneComando}.docx

./Chris Carter:
Chris Carter - Francezul 2.0 '{Politista}.docx

./Chris Kuzneski:
Chris Kuzneski - Semnul crucii 1.0 '{AventuraIstorica}.docx

./Chris Kyle:
Chris Kyle - Lunetistul american 1.0 '{ActiuneRazboi}.docx

./Chris Morgan:
Chris Morgan - Inaltarea de pe Alkaid 0.9 '{Diverse}.docx

./Chris Pavone:
Chris Pavone - Expatriatii 1.0 '{Thriller}.docx

./Chris Prentiss:
Chris Prentiss - Zen si arta fericirii 0.99 '{Spiritualitate}.docx

./Christel Zachert & Isabell Zachert:
Christel Zachert & Isabell Zachert - Ne revedem in paradisul meu 1.0 '{Literatura}.docx

./Christian Haller:
Christian Haller - Muzica inghitita 1.0 '{Literatura}.docx

./Christian Heidicker:
Christian Heidicker - Leac pentru o lume banala 1.0 '{Tineret}.docx

./Christian Jacq:
Christian Jacq - Afacerea Tutankhamon 1.0 '{AventuraIstorica}.docx
Christian Jacq - Ce dulce e viata la umbra palmierilor! 1.0 '{AventuraIstorica}.docx
Christian Jacq - Crima mumiei 1.0 '{AventuraIstorica}.docx
Christian Jacq - Din dragoste pentru Philae 1.0 '{AventuraIstorica}.docx
Christian Jacq - Locasul adevarului 1.0 '{AventuraIstorica}.docx
Christian Jacq - Misterele lui Osiris - V1 Arborele vietii 1.0 '{AventuraIstorica}.docx
Christian Jacq - Misterele lui Osiris - V2 Uneltirile raului 1.0 '{AventuraIstorica}.docx
Christian Jacq - Misterele lui Osiris - V3 Calea de foc 1.0 '{AventuraIstorica}.docx
Christian Jacq - Misterele lui Osiris - V4 Marele secret 1.0 '{AventuraIstorica}.docx
Christian Jacq - Paneb cel zelos 1.0 '{AventuraIstorica}.docx
Christian Jacq - Piatra Luminii - V1 Nefer cel tacut 1.0 '{AventuraIstorica}.docx
Christian Jacq - Piatra Luminii - V2 Femeia inteleapta 1.0 '{AventuraIstorica}.docx
Christian Jacq - Piatra Luminii - V3 Paneb cel zelos 1.0 '{AventuraIstorica}.docx
Christian Jacq - Ramses - V1 Fiul luminii 1.0 '{AventuraIstorica}.docx
Christian Jacq - Ramses - V2 Un templu pentru vesnicie 1.0 '{AventuraIstorica}.docx
Christian Jacq - Ramses - V3 Batalia de la Kadesh 1.0 '{AventuraIstorica}.docx
Christian Jacq - Ramses - V4 Doamna de la Abu Simbel 1.0 '{AventuraIstorica}.docx
Christian Jacq - Ramses - V5 La umbra arborelui de Acacia 1.0 '{AventuraIstorica}.docx
Christian Jacq - Razbunarea Zeilor - V1 Fugarul 1.0 '{AventuraIstorica}.docx
Christian Jacq - Razbunarea Zeilor - V2 Divina adoratoare 1.0 '{AventuraIstorica}.docx
Christian Jacq - Ultimul vis al Cleopatrei 1.0 '{AventuraIstorica}.docx

./Christian Leourier:
Christian Leourier - Omul care ucise iarna 1.0 '{SF}.docx

./Christina Baker Kline:
Christina Baker Kline - Trenul orfanilor 1.0 '{Literatura}.docx

./Christina Dalcher:
Christina Dalcher - Tacerea poate fi asurzitoare 1.0 '{SF}.docx
Christina Dalcher - Vox 1.0 '{SF}.docx

./Christina Dodd:
Christina Dodd - Cu pletele fluturand 0.99 '{Romance}.docx
Christina Dodd - Doamna in negru 1.0 '{Romance}.docx
Christina Dodd - Governess Brides - V1 Dragoste si abandon 1.0 '{Romance}.docx
Christina Dodd - Governess Brides - V2 Logodna in inalta societate 1.0 '{Romance}.docx
Christina Dodd - Governess Brides - V3 Legile iubirii 1.0 '{Romance}.docx
Christina Dodd - Governess Brides - V4 In cele mai nebunesti visuri 1.0 '{Romance}.docx
Christina Dodd - Governess Brides - V5 Ispita 1.0 '{Romance}.docx
Christina Dodd - Governess Brides - V6 Mireasa mea favorita 1.0 '{Romance}.docx
Christina Dodd - Governess Brides - V7 Arta iubirii 1.0 '{Romance}.docx
Christina Dodd - Governess Brides - V8 Suflete pereche 1.0 '{Romance}.docx
Christina Dodd - Governess Brides - V9 Aleasa printului 1.0 '{Romance}.docx
Christina Dodd - Scandalos! 1.0 '{Romance}.docx

./Christina Lane:
Christina Lane - Imprevizibila Colombine 0.99 '{Romance}.docx

./Christina Lauren:
Christina Lauren - Expertul seducator 1.0 '{Erotic}.docx
Christina Lauren - Strainul seducator 0.9 '{Romance}.docx

./Christine Alexander:
Christine Alexander - Infernul de pe frontul de est 1.0 '{Razboi}.docx

./Christine Angot:
Christine Angot - O iubire imposibila 1.0 '{Literatura}.docx

./Christine Cross:
Christine Cross - Ar trebui sa fii orb 0.99 '{Dragoste}.docx

./Christine Mangan:
Christine Mangan - Necunoscuta din Tanger 1.0 '{Literatura}.docx

./Christine Orban:
Christine Orban - Mesaj intarziat 0.99 '{Dragoste}.docx
Christine Orban - Vizitator de o seara 0.99 '{Romance}.docx

./Christofor Black:
Christofor Black - Pe urmele unui canibal 1.0 '{Politista}.docx

./Christophe Dufosse:
Christophe Dufosse - Sfarsitul orelor 0.7 '{Diverse}.docx

./Christopher Bollen:
Christopher Bollen - Prietenie tradata 1.0 '{Literatura}.docx

./Christopher Isherwood:
Christopher Isherwood - Adio, Berlin 0.99 '{Literatura}.docx

./Christopher Knight:
Christopher Knight - Inaintea piramidelor 1.0 '{MistersiStiinta}.docx

./Christopher Knight & Alan Butler:
Christopher Knight & Alan Butler - Prima civilizatie 1.0 '{MistersiStiinta}.docx

./Christopher Knight & Robert Lomas:
Christopher Knight & Robert Lomas - Aparatul lui Uriel. Originile antice ale stiintei 1.0 '{MistersiStiinta}.docx
Christopher Knight & Robert Lomas - Cartea lui Hiram 0.9 '{MistersiStiinta}.docx
Christopher Knight & Robert Lomas - Secretul lui Hiram 1.0 '{MistersiStiinta}.docx

./Christopher Marlowe:
Christopher Marlowe - Eduard al II-lea 0.6 '{Teatru}.docx
Christopher Marlowe - Evreul din Malta 0.6 '{Teatru}.docx
Christopher Marlowe - Tamerlan cel Mare 0.6 '{Teatru}.docx

./Christopher Paolini:
Christopher Paolini - Mostenirea - V1 Eragon 1.0 '{Supranatural}.docx
Christopher Paolini - Mostenirea - V2 Cartea primului nascut 1.0 '{Supranatural}.docx
Christopher Paolini - Mostenirea - V3 Brisingr 1.0 '{Supranatural}.docx
Christopher Paolini - Mostenirea - V4 Mostenirea 1.0 '{Supranatural}.docx

./Christopher Priest:
Christopher Priest - Lumea inversa 1.0 '{SF}.docx
Christopher Priest - Magicienii 1.0 '{SF}.docx

./Christopher Reich:
Christopher Reich - Clubul patriotilor 0.99 '{Thriller}.docx
Christopher Reich - Legea Conspiratiei 0.99 '{Thriller}.docx
Christopher Reich - Legea Razbunarii 0.99 '{Thriller}.docx

./Christopher Rowe:
Christopher Rowe - Statul voluntar 0.9 '{SF}.docx

./Christophor Black:
Christophor Black - Ingerul mortii 2.0 '{ActiuneComando}.docx
Christophor Black - O moarte traita din plin 1.0 '{ActiuneComando}.docx

./Christoph Ransmayr:
Christoph Ransmayr - Cox sau mersul timpului 1.0 '{Diverse}.docx

./Chuck Bainbridge:
Chuck Bainbridge - Cavalerii mortii 1.0 '{ActiuneComando}.docx
Chuck Bainbridge - Dreptatea mercenarilor 1.0 '{ActiuneComando}.docx

./Chuck Grossart:
Chuck Grossart - Aviatorii 1.0 '{SF}.docx

./Chuck Hogan:
Chuck Hogan - Incercuirea 1.0 '{Politista}.docx

./Chuck Norris:
Chuck Norris - Puterea secreta a sinelui 0.5 '{Spiritualitate}.docx

./Chuck Palahniuk:
Chuck Palahniuk - Bantuitii 1.0 '{Thriller}.docx
Chuck Palahniuk - Fight club 0.99 '{Thriller}.docx

./Cicero:
Cicero - Despre prietenie 0.99 '{Filozofie}.docx

./Cicerone Ionitoiu:
Cicerone Ionitoiu - Genocidul din Romania 0.7 '{Comunism}.docx

./Cilla & Rolf Borjlind:
Cilla & Rolf Borjlind - Maree inalta 1.0 '{Literatura}.docx

./Cincinat Pavelescu:
Cincinat Pavelescu - Epigrame 1.0 '{Epigrame}.docx

./Cindy I. Schulter:
Cindy I. Schulter - Mesaj de la Toth 0.2 '{Spiritualitate}.docx

./Cin Forshay Lundsford:
Cin Forshay Lundsford - Sa calci pe cenusa 0.9 '{Dragoste}.docx

./Cinghiz Aitmatov:
Cinghiz Aitmatov - Adio, floare galbena 1.0 '{Literatura}.docx
Cinghiz Aitmatov - Esafodul 1.0 '{Diverse}.docx
Cinghiz Aitmatov - O zi mai lunga decat veacul 1.1 '{Literatura}.docx
Cinghiz Aitmatov - Stigmatul Casandrei 1.0 '{Diverse}.docx
Cinghiz Aitmatov - Vaporul alb 1.0 '{Literatura}.docx

./Cip Iesan:
Cip Iesan - Mersul trenurilor 0.8 '{Umor}.docx

./Ciprian Mitoceanu:
Ciprian Mitoceanu - Afacerea Verei Nicolaevna 0.99 '{SF}.docx
Ciprian Mitoceanu - Amendamentul Dawson 0.2 '{SF}.docx
Ciprian Mitoceanu - Jurnalul unui animal de companie 0.99 '{SF}.docx
Ciprian Mitoceanu - Justitie de fier 0.99 '{SF}.docx
Ciprian Mitoceanu - O experienta stranie 0.99 '{SF}.docx
Ciprian Mitoceanu - O poveste perversa 0.99 '{SF}.docx
Ciprian Mitoceanu - Resurse nelimitate 0.99 '{SF}.docx
Ciprian Mitoceanu - Stare de bine 0.99 '{SF}.docx
Ciprian Mitoceanu - Triunghi mortal 0.99 '{SF}.docx
Ciprian Mitoceanu - Un rau necesar 0.9 '{SF}.docx

./Ciprian Valcan:
Ciprian Valcan - Compasul diavolului 0.99 '{Filozofie}.docx
Ciprian Valcan - Eseu despre metafizica teatrului 0.99 '{Filozofie}.docx
Ciprian Valcan - Filosofia pitonului 0.9 '{Filozofie}.docx
Ciprian Valcan - Fragmente 2009 0.99 '{Filozofie}.docx
Ciprian Valcan - Inchizitia sasaitilor 0.99 '{Filozofie}.docx
Ciprian Valcan - Madedonio Fernandez sau metafizica visului 0.99 '{Filozofie}.docx
Ciprian Valcan - Oameni pe catalige 0.99 '{Filozofie}.docx
Ciprian Valcan - Odiseu, Don Juan, Narcis 0.99 '{Filozofie}.docx
Ciprian Valcan - Sindicatul maimutelor 0.99 '{Filozofie}.docx
Ciprian Valcan - Sinucigasul si vremea nihilismului 0.99 '{Filozofie}.docx

./Claire Castillon:
Claire Castillon - Insecta 0.9 '{Literatura}.docx

./Claire Contreras:
Claire Contreras - Caleidoscopul inimilor 1.0 '{Romance}.docx

./Claire North:
Claire North - Atingerea 1.0 '{Fantasy}.docx

./Claire Valery:
Claire Valery - Ca prin farmec 0.99 '{Dragoste}.docx
Claire Valery - In ritmul inimilor 0.9 '{Dragoste}.docx
Claire Valery - Intalnire neasteptata 0.9 '{Dragoste}.docx
Claire Valery - Mireasa fugara 0.9 '{Romance}.docx

./Clara Nicollet:
Clara Nicollet - Arta alegerii 0.99 '{Politista}.docx
Clara Nicollet - Sancta simplicitas 0.9 '{Diverse}.docx

./Clara Sanchez:
Clara Sanchez - Misterul numelui tau 1.0 '{Literatura}.docx

./Clara Wimberly:
Clara Wimberly - Puterea primei iubiri 0.99 '{Dragoste}.docx

./Clare Gibson:
Clare Gibson - Semne si simboluri 0.9 '{Diverse}.docx

./Clare Harrison:
Clare Harrison - Marinarul solitar 0.9 '{Romance}.docx

./Clare Lavenham:
Clare Lavenham - Vacanta in Creta 0.9 '{Romance}.docx

./Clare Mackintosh:
Clare Mackintosh - Te las sa pleci 1.0 '{Thriller}.docx
Clare Mackintosh - Te vad 1.0 '{Thriller}.docx

./Clark Rawn:
Clark Rawn - Comentariu la Initiere in Hermetism 0.99 '{Spiritualitate}.docx
Clark Rawn - Corespondenta lui Rawn Clark la cartea Initiere in Hermetism 0.99 '{Spiritualitate}.docx

./Claude Anet:
Claude Anet - Mayerling 1.0 '{Dragoste}.docx

./Claude Dumont:
Claude Dumont - Paianjenii 0.6 '{SF}.docx

./Claude Mosse:
Claude Mosse - Procesul lui Socrate 0.99 '{Biografie}.docx

./Claude Roy:
Claude Roy - Intalnirea de pe Podul Artelor 1.0 '{Dragoste}.docx

./Claude Spaak:
Claude Spaak - Ecouri in memoria timpului 1.0 '{Dragoste}.docx
Claude Spaak - Marianne, frumoasa mea 0.9 '{Dragoste}.docx
Claude Spaak - Ordinea si dezordinea 1.0 '{Dragoste}.docx

./Claudia Befu:
Claudia Befu - Teatru 0.9 '{Teatru}.docx

./Claudia Golea:
Claudia Golea - Vara in Siam 0.9 '{Necenzurat}.docx

./Claudia Gray:
Claudia Gray - Evernight - V1 Evernight 1.0 '{Vampiri}.docx

./Claudia Jameson:
Claudia Jameson - Barbatul Scorpion 1.0 '{Romance}.docx

./Claudiu Nicolae Solcan:
Claudiu Nicolae Solcan - Indragostitii vorbesc despre vreme 0.99 '{Versuri}.docx

./Claudiu Paradais:
Claudiu Paradais - Cantecul Nibelungilor 1.0 '{Tineret}.docx

./Claus Cornelius Fischer:
Claus Cornelius Fischer - Si ne iarta noua greselile noastre 1.0 '{Politista}.docx

./Clayton Emery:
Clayton Emery - Proscrisii 1.0 '{SF}.docx

./Clelie Avit:
Clelie Avit - Sunt aici 1.0 '{Literatura}.docx

./Cliff Garnett:
Cliff Garnett - Talon Force - Fulgerul 1.0 '{ActiuneComando}.docx

./Clifford Donald Simak:
Clifford Donald Simak - Calea eternitatii 1.0 '{SF}.docx
Clifford Donald Simak - Factorul limita 0.99 '{SF}.docx
Clifford Donald Simak - Halta 1.0 '{SF}.docx

./Clifford Moore:
Clifford Moore - Al doilea amant al doamnei Chatterley 0.99 '{Dragoste}.docx

./Clive Barker:
Clive Barker - Cabal 1.0 '{Horror}.docx
Clive Barker - Cartile insangerate 2.0 '{Horror}.docx
Clive Barker - Intre dealuri, orasele 1.0 '{Horror}.docx

./Clive Cussler:
Clive Cussler - Dirk Pitt - V08 Cyclops 2.0 '{AventuraIstorica}.docx
Clive Cussler - Dirk Pitt - V09 Comoara 4.0 '{AventuraIstorica}.docx
Clive Cussler - Dirk Pitt - V10 Dragonul V1 4.0 '{AventuraIstorica}.docx
Clive Cussler - Dirk Pitt - V10 Dragonul V2 4.0 '{AventuraIstorica}.docx
Clive Cussler - Dirk Pitt - V11 Sahara 3.0 '{AventuraIstorica}.docx
Clive Cussler - Dirk Pitt - V12 Aurul incasilor 4.0 '{AventuraIstorica}.docx
Clive Cussler - Dirk Pitt - V16 Walhalla 1.0 '{AventuraIstorica}.docx
Clive Cussler - Dirk Pitt - V17 Odiseea troiana 3.0 '{AventuraIstorica}.docx
Clive Cussler - Dirk Pitt - V18 Vantul negru 2.0 '{AventuraIstorica}.docx
Clive Cussler - Dirk Pitt - V19 Comoara Marelui Han 2.0 '{AventuraIstorica}.docx
Clive Cussler - Dirk Pitt - V20 Moarte in Arctica 2.0 '{AventuraIstorica}.docx
Clive Cussler - Dirk Pitt - V26 Sageata lui Poseidon 1.0 '{AventuraIstorica}.docx
Clive Cussler - Isaac Bell - V1 Urmarirea 1.0 '{AventuraIstorica}.docx
Clive Cussler - Isaac Bell - V2 Sabotorul 1.0 '{AventuraIstorica}.docx

./Clive Cussler & Dirk Cussler:
Clive Cussler & Dirk Cussler - Zorii Semilunii 1.0 '{AventuraIstorica}.docx

./Clive Cussler & Grant Blackwood:
Clive Cussler & Grant Blackwood - Farago - V1 Aurul spartanilor 2.0 '{AventuraIstorica}.docx
Clive Cussler & Grant Blackwood - Farago - V2 Imperiul pierdut 1.0 '{AventuraIstorica}.docx
Clive Cussler & Grant Blackwood - Farago - V3 Regatul 2.0 '{AventuraIstorica}.docx

./Clive Cussler & Paul Kemprecos:
Clive Cussler & Paul Kemprecos - Nouma - V1 Sarpele 1.0 '{AventuraIstorica}.docx
Clive Cussler & Paul Kemprecos - Nouma - V2 Aurul albastru 2.0 '{AventuraIstorica}.docx
Clive Cussler & Paul Kemprecos - Nouma - V3 Gheata de foc 2.0 '{AventuraIstorica}.docx

./Clive Prince & Lynn Picknett:
Clive Prince & Lynn Picknett - Conspiratia Stargate 0.9 '{MistersiStiinta}.docx

./Clive Staples Lewis:
Clive Staples Lewis - Crestinism pur si simplu 0.8 '{Religie}.docx
Clive Staples Lewis - Crestinismul redus la esente 0.9 '{Religie}.docx
Clive Staples Lewis - Cronicile din Narnia - V1 Nepotul magicianului 5.0 '{Fantasy}.docx
Clive Staples Lewis - Cronicile din Narnia - V2 Sifonierul, leul si vrajitoarea 5.0 '{Fantasy}.docx
Clive Staples Lewis - Cronicile din Narnia - V3 Calul si baiatul 5.0 '{Fantasy}.docx
Clive Staples Lewis - Cronicile din Narnia - V4 Printul Caspian 5.0 '{Fantasy}.docx
Clive Staples Lewis - Cronicile din Narnia - V5 Calatorie pe mare 5.0 '{Fantasy}.docx
Clive Staples Lewis - Cronicile din Narnia - V6 Jiltul de argint 5.0 '{Fantasy}.docx
Clive Staples Lewis - Cronicile din Narnia - V7 Ultima batalie 5.0 '{Fantasy}.docx
Clive Staples Lewis - Despre minuni. Cele patru iubiri. Problema durerii 0.9 '{Religie}.docx
Clive Staples Lewis - Sfaturile unui diavol batran catre unul mai tanar 0.99 '{Religie}.docx

./Clorinda Matto de Turner:
Clorinda Matto de Turner - Pasari fara cuib. Suntem singuri dragostea mea 1.0 '{Dragoste}.docx

./Clynton Adams:
Clynton Adams - Triada X contraataca 0.99 '{SF}.docx

./Codrut Alexoaia:
Codrut Alexoaia - 36.9 vs 180 0.8 '{ProzaScurta}.docx

./Col Buchanan:
Col Buchanan - Farlander 1.0 '{SF}.docx

./Cole Daniel:
Cole Daniel - Ragdoll. Ultima ta zi 2.0 '{Thriller}.docx

./Coleridge:
Coleridge - Balada batranului marinar 1.0 '{Versuri}.docx

./Colin Campbell:
Colin Campbell - Integral 0.9 '{Sanatate}.docx
Colin Campbell - Studiul China 1.0 '{Sanatate}.docx

./Colin Falconer:
Colin Falconer - Anastasia 2.0 '{ActiuneComando}.docx
Colin Falconer - Aztec 4.0 '{ActiuneComando}.docx
Colin Falconer - Banzai 1.0 '{ActiuneComando}.docx
Colin Falconer - Ceasul mortii 1.0 '{ActiuneComando}.docx
Colin Falconer - Cleopatra, cand eram zei 2.0 '{ActiuneComando}.docx
Colin Falconer - Demonii 1.0 '{ActiuneComando}.docx
Colin Falconer - Frumoasa mea spioana 1.0 '{ActiuneComando}.docx
Colin Falconer - Furie 3.0 '{ActiuneComando}.docx
Colin Falconer - Harem 2.0 '{ActiuneComando}.docx
Colin Falconer - Infern 2.0 '{ActiuneComando}.docx
Colin Falconer - Opiu - V1 Opiu 2.0 '{ActiuneComando}.docx
Colin Falconer - Opiu - V2 Triada 2.0 '{ActiuneComando}.docx
Colin Falconer - Perle 2.0 '{ActiuneComando}.docx
Colin Falconer - Rafuiala 4.0 '{ActiuneComando}.docx
Colin Falconer - Risc 4.5 '{ActiuneComando}.docx
Colin Falconer - Teroare 2.0 '{ActiuneComando}.docx
Colin Falconer - Venin 2.0 '{ActiuneComando}.docx
Colin Falconer - Xanadu 2.0 '{ActiuneComando}.docx

./Colin Forbes:
Colin Forbes - Abis 2.0 '{Thriller}.docx
Colin Forbes - Ambuscada la Palermo 1.0 '{Thriller}.docx
Colin Forbes - Cacealmaua 2.0 '{Thriller}.docx
Colin Forbes - Fara crutare 1.0 '{Thriller}.docx
Colin Forbes - Fuhrerul si damnatii 1.0 '{Thriller}.docx
Colin Forbes - Furia sangelui 1.0 '{Thriller}.docx
Colin Forbes - Infruntarea 0.6 '{Thriller}.docx
Colin Forbes - Navele fantoma 1.0 '{Thriller}.docx
Colin Forbes - Obiectivul 5 1.0 '{Thriller}.docx
Colin Forbes - Omul cu doua fete 1.0 '{Thriller}.docx
Colin Forbes - Pe muchie de cutit 0.9 '{Thriller}.docx
Colin Forbes - Tweed & Co. - V02 Terminal 1.1 '{Thriller}.docx
Colin Forbes - Tweed & Co. - V07 Operatiunea Shockwave 1.0 '{Thriller}.docx
Colin Forbes - Tweed & Co. - V10 Crucea de foc 0.9 '{Thriller}.docx
Colin Forbes - Tweed & Co. - V11 Puterea 0.7 '{Thriller}.docx
Colin Forbes - Tweed & Co. - V13 Cataclismul 1.0 '{Thriller}.docx
Colin Forbes - Tweed & Co. - V18 Rinocerul 1.0 '{Thriller}.docx
Colin Forbes - Valul ucigas 1.0 '{Thriller}.docx
Colin Forbes - Vartejul 1.2 '{Thriller}.docx

./Colin P. Davies:
Colin P. Davies - Luptatorii 1.0 '{SF}.docx

./Colin Thubron:
Colin Thubron - Redescoperind Drumul Matasii. Din China si muntii Asiei Centrale in Iran si Turcia 1.0 '{Calatorii}.docx

./Colin Turner:
Colin Turner - Nascut pentru succes 0.99 '{DezvoltarePersonala}.docx

./Colleen Hoover:
Colleen Hoover - Fara speranta 0.99 '{Dragoste}.docx
Colleen Hoover - Slam din dragoste pentru Layken 0.99 '{Dragoste}.docx

./Colleen Mccullough:
Colleen Mccullough - Antoniu si Cleopatra 1.0 '{Dragoste}.docx
Colleen Mccullough - Deschis inchis 1.0 '{Dragoste}.docx
Colleen Mccullough - Doamnele din Missalonghi 1.0 '{Dragoste}.docx
Colleen Mccullough - Obsesie indecenta 1.0 '{Dragoste}.docx
Colleen Mccullough - Pasarea Spin 1.0 '{Dragoste}.docx
Colleen Mccullough - Tim 1.0 '{Dragoste}.docx

./Colum McCann:
Colum McCann - Treisprezece priviri diferite 0.99 '{Literatura}.docx

./Commander X:
Commander X - Controlorii lumii 1.0 '{MistersiStiinta}.docx
Commander X - Incredibilele tehnologii ale noii ordini sociale 0.9 '{MistersiStiinta}.docx

./Comornic:
Comornic - Adunare de Acatiste 0.99 '{Religie}.docx

./Connie Willis:
Connie Willis - Doomsday book 4.0 '{CalatorieinTimp}.docx
Connie Willis - Vanturile de la Marble Arch 1.0 '{SF}.docx

./Conn Iggulden:
Conn Iggulden - Imparatul - V1 La portile Romei 2.0 '{AventuraIstorica}.docx
Conn Iggulden - Imparatul - V2 Moartea regilor 1.0 '{AventuraIstorica}.docx
Conn Iggulden - War of the Roses - V1 Pasarea furtunii 1.0 '{AventuraIstorica}.docx
Conn Iggulden - War of the Roses - V2 Treimea 1.0 '{AventuraIstorica}.docx

./Conor Mcpherson:
Conor Mcpherson - Autoritati portuare 0.8 '{Diverse}.docx

./Conrad Jay Levinson:
Conrad Jay Levinson - Guerilla creativity 0.9 '{DezvoltarePersonala}.docx

./Constantin Acosmei:
Constantin Acosmei - Jucaria mortului 1.0 '{Versuri}.docx

./Constantin Arginteanu:
Constantin Arginteanu - Astrologia odinioara si azi 0.9 '{Spiritualitate}.docx

./Constantin Aslam:
Constantin Aslam - Istoria filosofiei 0.9 '{Filozofie}.docx

./Constantin Banu:
Constantin Banu - Umbra soarelui 0.7 '{Literatura}.docx

./Constantin Barbuceanu:
Constantin Barbuceanu - Expresul de noapte 1.0 '{Politista}.docx
Constantin Barbuceanu - Femei singure 1.0 '{Dragoste}.docx
Constantin Barbuceanu - Gentlemanul schiop 1.0 '{Politista}.docx
Constantin Barbuceanu - Samsarul 1.0 '{Literatura}.docx
Constantin Barbuceanu - Ultima spovedanie 1.0 '{Literatura}.docx

./Constantin Brancusi:
Constantin Brancusi - Aforisme 0.9 '{Aforisme}.docx

./Constantin Chirita:
Constantin Chirita - Ciresarii - V1 Cavalerii florii de cires 1.1 '{AventuraTineret}.docx
Constantin Chirita - Ciresarii - V2 Castelul fetei in alb 1.1 '{AventuraTineret}.docx
Constantin Chirita - Ciresarii - V3 Roata norocului 1.1 '{AventuraTineret}.docx
Constantin Chirita - Ciresarii - V4 Aripi de zapada 1.1 '{AventuraTineret}.docx
Constantin Chirita - Ciresarii - V5 Drum bun ciresari 1.1 '{AventuraTineret}.docx
Constantin Chirita - In Alb - V1 Trandafirul alb 1.1 '{AventuraTineret}.docx
Constantin Chirita - In Alb - V2 Pescarusul alb 1.0 '{AventuraTineret}.docx
Constantin Chirita - In Alb - V3 Ingerul alb 1.0 '{AventuraTineret}.docx
Constantin Chirita - Intalnirea V1 1.0 '{AventuraTineret}.docx
Constantin Chirita - Intalnirea V2 1.0 '{AventuraTineret}.docx
Constantin Chirita - Pasiuni 2.0 '{AventuraTineret}.docx

./Constantin Cozmiuc:
Constantin Cozmiuc - Acasa, prin nori 0.99 '{ProzaScurta}.docx
Constantin Cozmiuc - La poarta timpului 0.8 '{ProzaScurta}.docx
Constantin Cozmiuc - Poetul la masa de smarald 0.8 '{ProzaScurta}.docx

./Constantin Crasnobaiev:
Constantin Crasnobaiev - Fierbea oceanul 1.0 '{ClubulTemerarilor}.docx

./Constantin Cublesan:
Constantin Cublesan - Iarba cerului 1.0 '{SF}.docx
Constantin Cublesan - Nepasatoarele stele 2.0 '{SF}.docx
Constantin Cublesan - Paradoxala intoarcere 1.0 '{SF}.docx

./Constantin D. Nicolau:
Constantin D. Nicolau - Proba de foc. Amintiri 1.0 '{Razboi}.docx

./Constantin Daniel:
Constantin Daniel - Civilizatia Egiptului antic 1.0 '{Istorie}.docx
Constantin Daniel - Civilizatia Feniciana 1.0 '{Istorie}.docx
Constantin Daniel - Pe urmele vechilor civilizatii 1.0 '{Istorie}.docx

./Constantin Dobrogeanu Gherea:
Constantin Dobrogeanu Gherea - Neoiobagia 0.99 '{Istorie}.docx

./Constantin Fantana:
Constantin Fantana - Sentinta 1.0 '{Diverse}.docx

./Constantin George:
Constantin George - Poesii 0.99 '{Versuri}.docx

./Constantin Gheorghita:
Constantin Gheorghita - Info Reiki 0.9 '{Spiritualitate}.docx
Constantin Gheorghita - Manual Reiki 0.2 '{Spiritualitate}.docx
Constantin Gheorghita - Terapie spirituala - Ghid practic 0.4 '{Spiritualitate}.docx

./Constantin Gornea:
Constantin Gornea - Razboiul coclonilor 0.99 '{SF}.docx

./Constantin Husanu:
Constantin Husanu - Erotica 0.99 '{Erotic}.docx
Constantin Husanu - Pastile contra mortii 0.9 '{Diverse}.docx

./Constantin Ignatescu:
Constantin Ignatescu - Agurida 1.0 '{IstoricaRo}.docx

./Constantin Jinga:
Constantin Jinga - Biblia si sacrul in literatura 0.9 '{Diverse}.docx
Constantin Jinga - Lantul de la bicicleta. Lecturi biblice parohiale 0.9 '{Religie}.docx

./Constantin Luntraru:
Constantin Luntraru - A doua carte 0.99 '{Spiritualitate}.docx
Constantin Luntraru - Om inspre Inger - Cartea intai 0.8 '{Spiritualitate}.docx
Constantin Luntraru - Tatal si fiul 0.99 '{Spiritualitate}.docx

./Constantin Negruzzi:
Constantin Negruzzi - Alexandru Lapusneanu 1.0 '{IstoricaRo}.docx
Constantin Negruzzi - Amintiri de junete 1.0 '{IstoricaRo}.docx
Constantin Negruzzi - Aprodul Purice 1.0 '{IstoricaRo}.docx
Constantin Negruzzi - Muza de la Burdujani 1.0 '{IstoricaRo}.docx
Constantin Negruzzi - Negru pe alb 1.0 '{IstoricaRo}.docx

./Constantin Noica:
Constantin Noica - Cuvant impreuna despre rostirea romaneasca 0.9 '{Filozofie}.docx
Constantin Noica - De Caelo 0.99 '{Filozofie}.docx
Constantin Noica - Despartirea de Goethe 0.99 '{Filozofie}.docx
Constantin Noica - Despre lautarism 0.8 '{Filozofie}.docx
Constantin Noica - Mathesis sau bucuriile simple 0.99 '{Filozofie}.docx
Constantin Noica - Modelul cultural european 0.9 '{Filozofie}.docx

./Constantin Olteanu:
Constantin Olteanu - Istoria militara a romanilor in secolul XX 0.99 '{Istorie}.docx

./Constantin Onofrasi:
Constantin Onofrasi - Monahul si psihiatrul 0.9 '{Filozofie}.docx
Constantin Onofrasi - Omul 0.99 '{Filozofie}.docx

./Constantin Parfene:
Constantin Parfene - Mihai Eminescu. Note privind stilul publicisticii 0.9 '{Diverse}.docx

./Constantin Sanatescu:
Constantin Sanatescu - Jurnalul generalului Sanatescu 0.9 '{Jurnal}.docx

./Constantin Scarlat:
Constantin Scarlat - Tarmul nevazut al Marii Negre 0.7 '{Diverse}.docx

./Constantin Stamati:
Constantin Stamati - Domnia elefantului 1.0 '{Versuri}.docx
Constantin Stamati - Omul si cerul 1.0 '{Versuri}.docx

./Constantin Streia:
Constantin Streia - Haiducii 5.0 '{IstoricaRo}.docx

./Constantin Toiu:
Constantin Toiu - Galeria cu vita salbatica 1.0 '{Literatura}.docx

./Constantin Virgi Gheorghiu:
Constantin Virgi Gheorghiu - Ard malurile Nistrului 0.8 '{Istorie}.docx

./Constantin Virgil Banescu:
Constantin Virgil Banescu - Cainele, femeia si ocheada 0.9 '{Versuri}.docx

./Constantin Zarnescu:
Constantin Zarnescu - O antologie de povestiri Horror si Politiste - Crima Din Bayswater 1.1 '{Politista}.docx

./Cordwainer Smith:
Cordwainer Smith - Lorzii instrumentalitatii 0.9 '{SF}.docx

./Corina Ozon:
Corina Ozon - Noptile amantilor 0.9 '{Necenzurat}.docx
Corina Ozon - Zilele amantilor 0.9 '{Necenzurat}.docx

./Corin Braga:
Corin Braga - Oniria - Jurnal de vise 0.99 '{Jurnal}.docx

./Corinne Hofmann:
Corinne Hofmann - Adio, Africa 0.99 '{Literatura}.docx
Corinne Hofmann - Africa, dragostea mea 0.99 '{Literatura}.docx
Corinne Hofmann - Indragostita de un masai 0.9 '{Literatura}.docx
Corinne Hofmann - Revedere in Barsaloi 0.99 '{Literatura}.docx

./Cormac Mccarthy:
Cormac Mccarthy - Calutii mei, caluti frumosi 1.0 '{Aventura}.docx
Cormac Mccarthy - Drumul 1.0 '{Aventura}.docx
Cormac Mccarthy - Meridianul sangelui 1.0 '{Aventura}.docx
Cormac Mccarthy - Nu exista tara pentru batrani 1.0 '{Aventura}.docx

./Cornel Armeanu:
Cornel Armeanu - Apel catre intreaga natiune romana 0.99 '{ProzaScurta}.docx
Cornel Armeanu - Despre Cornel Armeanu 0.99 '{ProzaScurta}.docx
Cornel Armeanu - Mireasa mea din Alcatraz 0.99 '{ProzaScurta}.docx
Cornel Armeanu - Transilvania 0.99 '{ProzaScurta}.docx

./Cornel Brahas:
Cornel Brahas - Mortii nu mai stiu drumul spre casa 2.0 '{Razboi}.docx

./Cornel Calugaru:
Cornel Calugaru - Ramasag pe un sarut 1.0 '{Politista}.docx

./Cornel Hodojeu:
Cornel Hodojeu - Albatros-31 1.0 '{Politista}.docx

./Cornelia Funke:
Cornelia Funke - Inima de cerneala 0.7 '{Diverse}.docx

./Corneliu Beda:
Corneliu Beda - Donaris se razbuna 1.0 '{ClubulTemerarilor}.docx
Corneliu Beda - Taina cetatii 1.0 '{ClubulTemerarilor}.docx

./Corneliu Buzinschi:
Corneliu Buzinschi - Nuanta albastra a caderii 0.7 '{Politista}.docx

./Corneliu Florea:
Corneliu Florea - Patibularul Patapievici 0.7 '{Diverse}.docx

./Corneliu Leu:
Corneliu Leu - Insulele 0.99 '{Dragoste}.docx

./Corneliu Omescu:
Corneliu Omescu - Planeta fara memorie 1.0 '{SF}.docx

./Corneliu Radu Cadelcu:
Corneliu Radu Cadelcu - Detectivii de duminica 1.0 '{Politista}.docx

./Cornelius Ryan:
Cornelius Ryan - Un pod prea indepartat 1.0 '{ActiuneRazboi}.docx

./Corneliu Stefan:
Corneliu Stefan - Drumul prin padure 0.6 '{Diverse}.docx

./Corneliu Stefanache:
Corneliu Stefanache - Ruptura 0.6 '{Diverse}.docx
Corneliu Stefanache - Ziua uitarii 1.0 '{Dragoste}.docx

./Corneliu Vadim Tudor:
Corneliu Vadim Tudor - Lectia de istorie. La pupitru C.V.T. 0.99 '{Istorie}.docx

./Cornel Marginean:
Cornel Marginean - Omul cel trist 0.99 '{SF}.docx

./Cornel Mihai Ungureanu:
Cornel Mihai Ungureanu - Cursa speciala 0.9 '{ProzaScurta}.docx
Cornel Mihai Ungureanu - Doctoria 0.99 '{ProzaScurta}.docx
Cornel Mihai Ungureanu - Drumul 0.99 '{ProzaScurta}.docx
Cornel Mihai Ungureanu - Fluture 0.9 '{ProzaScurta}.docx
Cornel Mihai Ungureanu - Pasii sarpelui 0.99 '{ProzaScurta}.docx
Cornel Mihai Ungureanu - Piatra 0.99 '{ProzaScurta}.docx
Cornel Mihai Ungureanu - Singuratate 0.99 '{ProzaScurta}.docx
Cornel Mihai Ungureanu - Treptele din fata casei 0.99 '{ProzaScurta}.docx

./Cornel Scafes:
Cornel Scafes - Partida de puzzle 1.0 '{Politista}.docx

./Cory Doctorow:
Cory Doctorow - Cand Sysadminii conduceau lumea 1.0 '{SF}.docx
Cory Doctorow - Eu, robo barca 2.0 '{SF}.docx
Cory Doctorow - Little Brother 0.8 '{AventuraTineret}.docx

./Cosma Brasoveanu:
Cosma Brasoveanu - Omul fara umbra 2.0 '{Politista}.docx
Cosma Brasoveanu - Un martor incomod 1.0 '{Politista}.docx

./Cosmin Dragomir:
Cosmin Dragomir - Made in China 0.9 '{ProzaScurta}.docx

./Cosmin Lascu:
Cosmin Lascu - Immortal - Razboiul continua 0.9 '{SF}.docx

./Cosmin Stefanescu:
Cosmin Stefanescu - Labirint apocaliptic 0.8 '{Diverse}.docx

./Costache Caragata:
Costache Caragata - Caietele tristetii 0.8 '{Memorii}.docx

./Costache Conachi:
Costache Conachi - Visul amoriului 1.0 '{Versuri}.docx

./Costel Babos:
Costel Babos - Calipsul 0.99 '{ProzaScurta}.docx
Costel Babos - Petru din sticla 0.99 '{ProzaScurta}.docx
Costel Babos - Poveste neterminata cu o pisica moarta 0.9 '{ProzaScurta}.docx
Costel Babos - Povestind in drum spre oglinda 0.8 '{ProzaScurta}.docx
Costel Babos - Rezervatia 0.9 '{ProzaScurta}.docx
Costel Babos - Soldatul V1 0.9 '{ProzaScurta}.docx
Costel Babos - Soldatul V2 0.9 '{ProzaScurta}.docx
Costel Babos - Un om din Wayfalua 0.9 '{ProzaScurta}.docx

./Costel Pricopie:
Costel Pricopie - Cuvioasa Lindia 1.0 '{Erotic}.docx

./Costi Gurgu:
Costi Gurgu - Ciuma de sticla 0.9 '{SF}.docx
Costi Gurgu - Minunata Caoara 0.9 '{SF}.docx

./Costin Merisca:
Costin Merisca - Tragedia Pitesti 0.6 '{Jurnal}.docx

./Cotizo Draia:
Cotizo Draia - Genotip de lux 0.99 '{SF}.docx
Cotizo Draia - Hienele 0.99 '{SF}.docx
Cotizo Draia - Un tablou de Hatshepsut 0.99 '{SF}.docx
Cotizo Draia - Vineri in dimensiuni diferite 0.99 '{SF}.docx

./Craig Russell:
Craig Russell - Jan Fabel - V1 Vulturul insangerat 1.0 '{Politista}.docx
Craig Russell - Jan Fabel - V2 Fratele Grimm 1.0 '{Politista}.docx
Craig Russell - Jan Fabel - V3 Rosu pentru totdeauna 1.0 '{Politista}.docx

./Craig Thomas:
Craig Thomas - Firefox 1.0 '{ActiuneComando}.docx
Craig Thomas - Firefox down 1.0 '{ActiuneComando}.docx
Craig Thomas - Leopardul de mare 1.0 '{ActiuneComando}.docx

./Crina Decuseara Bocsan:
Crina Decuseara Bocsan - Povestiri despre Vlad Tepes 1.0 '{IstoricaRo}.docx

./Cristache Gheorghiu:
Cristache Gheorghiu - Intre doua idealuri 0.9 '{Diverse}.docx
Cristache Gheorghiu - Memoriile unui catel adult 0.99 '{Diverse}.docx
Cristache Gheorghiu - Singur printre americani 0.99 '{Diverse}.docx
Cristache Gheorghiu - Traditie sau liberul arbitru 0.99 '{Diverse}.docx

./Cristian Florin Popescu:
Cristian Florin Popescu - Manual jurnalism 0.9 '{Jurnalism}.docx

./Cristian Ganescu:
Cristian Ganescu - Forta contra Forta - V1 Marturiile unui clarvazator 0.9 '{Spiritualitate}.docx
Cristian Ganescu - Forta contra Forta - V2 Omul in afara trupului 0.9 '{Spiritualitate}.docx
Cristian Ganescu - Forta contra Forta - V3 Marele mister 0.9 '{Spiritualitate}.docx
Cristian Ganescu - Forta contra Forta - V4 In fata eternitatii 0.9 '{Spiritualitate}.docx
Cristian Ganescu - Kosmos 0.7 '{Spiritualitate}.docx
Cristian Ganescu - Tainele initiatilor vechiului Egipt 0.7 '{Spiritualitate}.docx

./Cristian Ghinea:
Cristian Ghinea - Cap de tarnacop 0.99 '{ProzaScurta}.docx
Cristian Ghinea - Slabanoaga 0.99 '{ProzaScurta}.docx

./Cristian Koncz:
Cristian Koncz - Rochia fetitei cu buline albastre 0.99 '{ProzaScurta}.docx

./Cristian M. Teodorescu:
Cristian M. Teodorescu - Basme africane 0.99 '{ProzaScurta}.docx
Cristian M. Teodorescu - Bing, bing, Larisa 0.7 '{ProzaScurta}.docx
Cristian M. Teodorescu - Electromagneto-muza 0.9 '{ProzaScurta}.docx
Cristian M. Teodorescu - Electronii sunt mai destepti decat noi 0.9 '{ProzaScurta}.docx
Cristian M. Teodorescu - Majoratul Segolenei 0.9 '{Literatura}.docx
Cristian M. Teodorescu - Moartea domnului Teodorescu 0.99 '{ProzaScurta}.docx
Cristian M. Teodorescu - Pisica treispe 0.9 '{Teatru}.docx

./Cristian Moisescu:
Cristian Moisescu - Devieri de la calea vietii 0.9 '{Religie}.docx

./Cristian Negrea:
Cristian Negrea - Cand armele vorbesc 1.0 '{ActiuneRazboi}.docx
Cristian Negrea - Cnutul maicii Rusia 1.0 '{ActiuneRazboi}.docx
Cristian Negrea - Sange pe Nistru 1.0 '{ActiuneRazboi}.docx

./Cristian Negureanu:
Cristian Negureanu - Civilizatiile extraterestre si a treia conflagratie mondiala 0.9 '{MistersiStiinta}.docx
Cristian Negureanu - Intraterestrii si noua ordine mondiala 1.0 '{MistersiStiinta}.docx
Cristian Negureanu - Sosirea zeilor 1.0 '{MistersiStiinta}.docx

./Cristian Troncota:
Cristian Troncota - Omul de taina al Maresalului 1.0 '{Istorie}.docx

./Cristian Tudor Popescu:
Cristian Tudor Popescu - Cassargoz 0.99 '{Literatura}.docx
Cristian Tudor Popescu - Copii fiarei 1.0 '{Literatura}.docx
Cristian Tudor Popescu - Libertatea de a uri 0.99 '{Literatura}.docx
Cristian Tudor Popescu - Nobelul romanesc 1.0 '{Literatura}.docx
Cristian Tudor Popescu - Planetarium 1.0 '{Literatura}.docx
Cristian Tudor Popescu - Timp mort 0.99 '{Literatura}.docx
Cristian Tudor Popescu - Un cadavru umplut cu ziare 0.9 '{Literatura}.docx
Cristian Tudor Popescu - Vremea manzului sec 0.99 '{Literatura}.docx

./Cristi Mitran:
Cristi Mitran - Plimbarea catre casa 0.99 '{Diverse}.docx

./Cristina Alger:
Cristina Alger - Sotia bancherului 1.0 '{Literatura}.docx

./Cristina Chiperi:
Cristina Chiperi - My dilemma is you V1 1.0 '{AventuraTineret}.docx

./Cristina Coman:
Cristina Coman - R.P. - Principii si strategii 0.9 '{Diverse}.docx
Cristina Coman - Relatii publice si massmedia 0.9 '{Diverse}.docx

./Cristina Jinga:
Cristina Jinga - Calatoriile lui Marco Polo 0.8 '{Istorie}.docx

./Cristina Rotaru:
Cristina Rotaru - Iar frunzele se invarteau nebuneste 0.8 '{ProzaScurta}.docx
Cristina Rotaru - Urmele trecerii tale 0.8 '{ProzaScurta}.docx

./Cristopher Marlowe:
Cristopher Marlowe - Tragica istorie a doctorului Faust 0.7 '{Teatru}.docx

./Cristopher Reich:
Cristopher Reich - Legea conspiratiei 1.0 '{Thriller}.docx

./Culegere de schite si nuvele:
Culegere de schite si nuvele - Repauzatul inspector Narbon actioneaza 2.0 '{Politista}.docx

./Curtis C. Chen:
Curtis C. Chen - In valuri 1.0 '{SF}.docx

./Curzio Malaparte:
Curzio Malaparte - Kaputt 1.0 '{Razboi}.docx
Curzio Malaparte - Pielea 1.0 '{Razboi}.docx
Curzio Malaparte - Tehnica loviturii de stat 0.99 '{Politica}.docx

./Cynthia D'Aprix Sweeney:
Cynthia D'Aprix Sweeney - Mostenirea 1.0 '{Literatura}.docx

./Cynthia Freeman:
Cynthia Freeman - Iluzia iubirii 0.99 '{Dragoste}.docx

./Cynthia Harrod Eagles:
Cynthia Harrod Eagles - Pe aripile iubirii 0.99 '{Dragoste}.docx

./Cynthia Kadohata:
Cynthia Kadohata - Kira Kira 1.0 '{Tineret}.docx

./Cynthia Phillips & Shana Priwer:
Cynthia Phillips & Shana Priwer - 101 lucruri inedite despre Einstein 2.0 '{Istorie}.docx

./Cynthia Powell:
Cynthia Powell - Erou de inchiriat 0.99 '{Dragoste}.docx
Cynthia Powell - Ingerul negru 0.99 '{Dragoste}.docx
Cynthia Powell - Planul de rezerva 0.8 '{Romance}.docx
Cynthia Powell - Vaduva 0.99 '{Dragoste}.docx

./Cynthia Van Rooy:
Cynthia Van Rooy - Joc periculos 0.99 '{Dragoste}.docx

./Cynthia Ward:
Cynthia Ward - Ultima zi de scoala 0.9 '{SF}.docx

./Cyril Massarotto:
Cyril Massarotto - Dumnezeu mi-e bun amic 0.99 '{Literatura}.docx
```

